<?php
session_start();

function verifconnex() {
if(!isset($_SESSION['login'])) {
  header("Location:index.php?message=authentifiez vous pour accéder a cette page");
  exit;}
}

function verifadmin() {
    if( $_SESSION['role']!='gestionnaire'){
     header("Location:index.php?message=vous n'avez pas les droits pour accéder a cette page");
     exit;}
}
function veriffourni() {
    if( $_SESSION['role']=='client'){
     header("Location:index.php?message=vous n'avez pas les droits pour accéder a cette page");
     exit;}
}
?>  